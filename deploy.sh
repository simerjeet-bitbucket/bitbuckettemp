#!/bin/bash
sudo apt-get install jq

content=$(wget http://production-review-tool.herokuapp.com/api/checkReadyToDeploy?app_name=bitbucketTemp -q -O -)

echo $content;  
status=$(echo $content | jq '.success' )
echo $status; 
if [ $status == 'true' ]; then
 exit 0
fi
exit 1